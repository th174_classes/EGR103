% Oscillations.m
% Tianyue Huang
% 10/5/2015

% I have adhered to all the tenets of the 
% Duke Coddmmunity Standard in creating this code.
% Signed: th174
%%
clear;
format short e
[t,tau] = meshgrid(linspace(0,2*pi,50),0.5:0.5:10);
z = exp(-t./tau).*sin(2.*t);
figure(1)
colormap jet
surf(t,tau,z);
xlabel 'Time (s)'
ylabel 'Time Constant (s)'
title 'Plot of e^{-t/\tau}sin(2t)'
print -depsc Oscillations1
figure(2)
colormap jet
contour(t,tau,z,-1:.1:1);
xlabel 'Time (s)'
ylabel 'Time Constant (s)'
axis equal
print -depsc Oscillations2
