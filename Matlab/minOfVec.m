function min = minOfVec(A)
min = A(1);
for i=2:length(A)
    if i<min
        min = i;
    end
end