clear
k = 24;
r = 1.6;
s = 0.5;
u = 0.75;
v = 1.8;
%p1(1) = 1;
%q1(1) = 2;
p2(1) = 18;
q2(1) = 6;
for n = 1:999
    %p1(n+1) = p1(n) + r*p1(n) * (1-p1(n)/k) - s*p1(n)*q1(n);
    %q1(n+1) = (1-u)*q1(n) + v*p1(n)*q1(n);
    p2(n+1) = p2(n)*exp(r*(1-p2(n)/k) - s*q2(n));
    q2(n+1) = (1-u)*(1-exp(-v*q2(n)))*p2(n);
end
subplot(2,1,1)
%plot(p1, 'g-');
hold on
plot(p2, 'k-.');
%plot(q1, 'r-');
plot(q2, 'k-');
grid on
legend('Prey','Predator')
xlabel 'Generation, n'
ylabel 'Population, f(n)'
title 'Model of predator and prey populations over time'
subplot(2,1,2)
%plot(p1,q1);
hold on
plot(p2,q2,'k');
grid on
legend 'Phase plane curve'
xlabel 'Population of Prey, P(n)'
ylabel 'Population of Predator, Q(n)'