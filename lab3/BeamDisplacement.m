% BeamDisplacement.m
% Tianyue Huang
% 09/17/2015

% I have adhered to all the tenets of the 
% Duke Community Standard in creating this code.
% Signed: th174

% Honor Code

%% Initialize the workspace
clear;
format compact;
format short e;

%% Genreate values and plot them
%  Generate 100 sample points for x, define Singularity, then
%  calculate the displacement based on those 100 points
x = linspace(0,10,100);
Singularity = @(x,a,n) (x > a).*(x-a).^n;
displacement = -(5/6).*(Singularity(x,0,4)-Singularity(x,5,4))+(5/2).*Singularity(x,8,3)+(285/2).*Singularity(x,7,2)+(29/4).*(x.^3)-74.*x;
%  Initialize the plot, plot the values, and add
%  labels, a title, and a grid.  Print the plot
plot(x,displacement,'-')
hold on
grid on
xlabel 'Distance along beam(feet)'
ylabel 'Displacement'
title 'Deflection of a Beam with 285 kip-ft of Torque (th174)'
print -deps DisplacementPlot

%% Generate more precise values for min/max determination
%  Generate 1e6 sample points for x, then
%  calculate the displacement based on those 1e6 points
%  Note - Singularity was already defined above, just use it
%  Determine most positive and negative displacements and location
x=linspace(0,10,1e6);
displacement = -(5/6).*(Singularity(x,0,4)-Singularity(x,5,4))+(5/2).*Singularity(x,8,3)+(285/2).*Singularity(x,7,2)+(29/4).*(x.^3)-74.*x;
[yMin] = min(displacement)
[yMax] = max(displacement)

