% RunConc.m
% Tianyue Huang
% 9/17/2015

% I have adhered to all the tenets of the
% Duke Community Standard in creating this code.
% Signed: th174

%% Initialize workspace
clear;
format compact;
format short e
%% Store values from table in variables
t1 = 10:10:60;
c1 = [3.4,2.6,1.6,1.3,1.0,0.5];
%% Create time base and concentration calculations
%  from equation
t2 = linspace(0,60,1000);
c2 = 4.84*exp(-0.034*t2);
%% Plot the data points using solid red diamonds,
%  then add the model using a dashed green line
plot(t1,c1,'rd')
hold on
plot(t2,c2,'g--')
hold on
%% Add labels and a title, then print in color
grid on
xlabel 'Time (min)'
ylabel 'Concentration of Aqueous Bromine (ppm)'
title 'Photodegradation of Aqueous Bromine over Time (th174)'
print -depsc ConcPlot