% TestSingularity.m
% Tianyue Huang
% 9/17/2015
% Based on: TestSingularity.m
% Written by: Dr. Gustafson

% I have adhered to all the tenets of the 
% Duke Community Standard in creating this code.
% Signed: th174

%% Change the code in lines 14 and 21

clear; format short e
Singularity = @(x,a,n) (x > a).*(x-a).^n;
x  = linspace(-2, 5, 500);
plot(x, Singularity(x, -1, 0), 'k-',...
     x, Singularity(x,  0, 1), 'k--',...
     x, Singularity(x,  1, 1), 'k-.',...
     x, Singularity(x,  3, 2), 'k:');
legend('<x+1>^0', '<x>^1', '<x-1>^1', '<x-3>^2', 0)
title('Four Different Values of y=<x-a>^n (th174)'); 
xlabel('x');
ylabel('y');
grid off
print -deps SingPlots