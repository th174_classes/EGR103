% PlotExponential.m
% Tianyue Huang
% 9/25/2015

% I have adhered to all the tenets of the 
% Duke Coddmmunity Standard in creating this code.
% Signed: th174
%%
clear;
xValues = linspace(0,1,1000);
m = -2:2;
yValues = 10.^(m'*xValues);
plot(xValues,yValues,'k-');
axis([0,1,0,4])
xt = [.09,.15,.44,.51,.53];
yt = [.25,3.6,3.5,.42,1.2];
str = {'{\itm} = -2','{\itm} = 2','{\itm} = 1','{\itm} =  -1','{\itm} = 0'};
text(xt,yt,str);
xlabel {\itx};
ylabel {\ity};
title 'The Exponential Function {\ity} = 10^{\itmx} (th174)'
print -deps PlotExponential
