function [x, y] = Butterfly(t)
% Butterfly.m
% Tianyue Huang
% 9/17/2015
% I have adhered to all the tenets of the 
% Duke Community Standard in creating this code.
% Signed: th174
%% Equations for x and for y in terms of t here
x = sin(t).*(2.718281828459045.^cos(t)-2.*cos(4.*t)-sin(t/12).^5);
y = cos(t).*(2.718281828459045.^cos(t)-2.*cos(4.*t)-sin(t/12).^5);