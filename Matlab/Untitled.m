xValues = linspace(0,5,1000);
y1 = exp(-1.*xValues);
y2 = 1-exp(-1.*xValues);
plot (xValues, y1, 'k-', xValues, y2, 'k--')
legend('e^{- x}', '1 - e^{- x}');
title 'Graphs of Exponential Functions'
xlabel x
ylabel y
grid on