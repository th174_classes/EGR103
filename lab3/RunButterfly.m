% RunButterfly.m
% Tianyue Huang
% 9/17/2015

% I have adhered to all the tenets of the 
% Duke Community Standard in creating this code.
% Signed: th174

%% Initialize workspace
clear;
format compact;
format short e
%% Set up variable to store times and use Butterfly 
%  function to get vectors for x and y coordinates
t = linspace(0,100,2000);
%% Make plot, add grid, labels and titles, then print
[x,y] = Butterfly(t);
plot(x,y,'k-')
grid on
title 'Parametric Butterfly Plot (th174)'
xlabel 'x(t)'
ylabel 'y(t)'
print -deps ButterflyPlot % You're welcome!