% LiquidFlow.m
% Tianyue Huang
% 10/5/2015

% I have adhered to all the tenets of the 
% Duke Coddmmunity Standard in creating this code.
% Signed: th174
%%
clear;
format short e
S = .0003;
n = .022;
[B,H] = meshgrid(linspace(.01,50,20),linspace(.01,50,21));
U = sqrt(S)/n.*((B.*H)./(B+2.*H)).^(2/3);
surfc(B,H,U);
xlabel 'Width (m)'
ylabel 'Depth (m)'
zlabel 'Velocity (m/s)'
title 'Velocity of Water in a Rectangular Channel (th174)'
grid on
colormap copper
colorbar 
print -depsc LiquidFlow

