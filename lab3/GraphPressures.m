% GraphPressures.m
% Tianyue Huang
% 9/17/2015

% I have adhered to all the tenets of the 
% Duke Community Standard in creating this code.
% Signed: th174

%% Initialize the workspace
clear;
format compact;
format short e;
%% Create vector of volumes and vectors for each gas
vol = linspace(1,2,200);
temp = 300;
%% Start and clear figure
clf;
figure(1);
%% Set increment for point plots and plot points and lines
hold on
y1 =VanDerWaals(temp, vol, 'Helium');
y2 = VanDerWaals(temp, vol, 'Hydrogen');
y3 = VanDerWaals(temp, vol, 'Oxygen');
y4 = VanDerWaals(temp, vol, 'Carbon dioxide');
y5 = VanDerWaals(temp, vol, 'Chlorine');
y6 = VanDerWaals(temp, vol, 'Water vapour');
plot(vol, y1);
plot(vol, y2);
plot(vol, y3);
plot(vol, y4);
plot(vol, y5);
plot(vol, y6);
Indices = 1:12:length(vol);
PointPlot=plot(...
    vol(Indices), y1(Indices), '+',...
    vol(Indices), y2(Indices), 'o',...
    vol(Indices), y3(Indices), 'p',...
    vol(Indices), y4(Indices), '*',...
    vol(Indices), y5(Indices), '^',...
    vol(Indices), y6(Indices), 's');
hold off
%% Add title, labels, then print
legend(PointPlot,'Helium','Hydrogen','Oxygen','Carbon dioxide','Chlorine','Water vapour');
title 'Pressure and Volume of Non-Ideal Gases at 300K (th174)'
xlabel 'Volume (Liters)'
ylabel 'Pressure (Atmospheres)'
print -deps PressuresPlot