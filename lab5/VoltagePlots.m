% VoltagePlots.m
% Tianyue Huang
% 10/5/2015

% I have adhered to all the tenets of the 
% Duke Coddmmunity Standard in creating this code.
% Signed: th174
%%
clear;
format short e
time = 0:0.5:4;
voltage = [100 62 38 21 13 7 4 2 3];
subplot(3,1,1);
stem(time,voltage,'k');
gzoom;
xlabel 'Time'
ylabel 'Voltage'
title 'Voltage Decay over time (th174)'
subplot(3,1,2);
bar(time,voltage,'k');
axis([-.4 4.4 -10 110]);
xlabel 'Time'
ylabel 'Voltage'
subplot(3,1,3);
stairs(time,voltage,'k');
gzoom;
xlabel 'Time'
ylabel 'Voltage'
orient tall
print -deps VoltagePlots
orient portrait
