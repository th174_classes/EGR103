% IsometricProjection.m
% Tianyue Huang
% 9/25/2015
% I have adhered to all the tenets of the 
% Duke Coddmmunity Standard in creating this code.
% Signed: th174
%% Initialize
t = linspace(-100,100,100000);
xt = 10.*exp(-0.5.*t).*sin(3.*t+2);
yt = 7.*exp(-0.4.*t).*cos(5.*t-3);
zt = (8./5).*t-8;
%% Plot Everything
subplot(2,2,1)
plot(xt,yt,'k-');
axis([-10 10 -10 10])
grid on
xlabel x
ylabel y
subplot(2,2,2)
plot3(xt,yt,zt,'k-')
axis([-10 10 -10 10 -10 10])
grid on
xlabel x
ylabel y
zlabel z
title 'Isometric Plots (th174)'
view(45,35);
subplot(2,2,3)
plot(xt,zt,'k-');
axis([-10 10 -10 10])
grid on
xlabel x
ylabel z
subplot(2,2,4)
plot(yt,zt,'k-');
axis([-10 10 -10 10])
grid on
xlabel y
ylabel z
%% Print
print -deps IsometricProjection