% ContourPlots.m
% Tianyue Huang
% 10/5/2015

% I have adhered to all the tenets of the 
% Duke Coddmmunity Standard in creating this code.
% Signed: th174
%%
clear;
format short e
[x,y] = meshgrid(linspace(-1,1,25),linspace(-1,1,25));
z1 = x.^2-2.*x.*y+4.*y.^2;
z2 = -x.^2+2.*x.*y+3.*y.^2;
z3 = (x-y.^2).*(x-3.*y.^2);
%%
figure(1);
colormap jet
subplot(1,2,1);
surfc(x,y,z1);
xlabel 'x value'
ylabel 'y value'
zlabel 'z value'
title 'Plot of z = x^2-2xy+4y^2'
subplot(1,2,2);
contour(x,y,z1,50);
axis equal
xlabel 'x value'
ylabel 'y value'
zlabel 'z value'
print -depsc ContourPlot1
%%
figure(2);
colormap jet
subplot(1,2,1);
surfc(x,y,z2);
xlabel 'x value'
ylabel 'y value'
zlabel 'z value'
title 'Plot of z = -x^2+2xy+3y^2'
subplot(1,2,2);
contour(x,y,z2,50);
axis equal
xlabel 'x value'
ylabel 'y value'
zlabel 'z value'
print -depsc ContourPlot2
%%
figure(3);
colormap jet
subplot(1,2,1);
surfc(x,y,z3);
xlabel 'x value'
ylabel 'y value'
zlabel 'z value'
title 'Plot of z = (x-y^2)(x-3y^2)'
subplot(1,2,2);
contour(x,y,z3,50);
axis equal
xlabel 'x value'
ylabel 'y value'
zlabel 'z value'
print -depsc ContourPlot3