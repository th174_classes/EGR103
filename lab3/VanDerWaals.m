function Pressure = VanDerWaals(Temp, Vol, Gas)
% VanDerWaals  Calculate pressures for a gas.
%   Pressure = VanDerWaals(Temp, Vol, Gas)
%     Temp: a matrix of temperatures
%     Vol: a matrix of specific volumes
%     Gas: a string with the name of a gas
% VanDerWaals.m
% Tianyue Huang
% 9/17/2015
% I have adhered to all the tenets of the 
% Duke Community Standard in creating this code.
% Signed: th174

%% Use switch tree to determine gas and a and b values
switch Gas
    case {'Helium', 'HE'}
        a = 0.0341;
        b = 0.0237;
    case {'Hydrogen', 'H2'}
        a = 0.244;
        b = 0.0266;
    case {'Oxygen' ,'O2'}
        a = 1.36;
        b = 0.0318;
    case {'Chlorine', 'Cl2'}
        a = 6.49;
        b = 0.0562;
    case {'Carbon dioxide', 'CO2'}
        a = 3.59;
        b = 0.0427;
    case {'Water vapour', 'H2O'}
        a = 5.536;
        b = 0.03049;
        % a and b values from 
        %Weast. R. C. (Ed.), Handbook of Chemistry and Physics (53rd Edn.), Cleveland:Chemical Rubber Co., 1972.
    otherwise
        error('Gas not in database!');
end
%% Use formula to calculate array of pressures for that gas
Pressure = 0.08206*Temp./(Vol - b)-a./(Vol).^2;
end