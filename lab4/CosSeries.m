% CosSeries.m
% Tianyue Huang
% 9/25/2015

% I have adhered to all the tenets of the 
% Duke Coddmmunity Standard in creating this code.
% Signed: th174
%%
function [CosApprox,RelError] = CosSeries(x,N)
k = sym('integer');
for i = 1:N
    CosApprox(i) = symsum((-1).^k.*x.^(2.*k)./factorial(2.*k),k,[0,i-1]);
    RelError(i) = (cos(x)-CosApprox(i))/cos(x)*100;
end
    
