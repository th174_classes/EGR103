% BreakEvenPoint.m
% Tianyue Huang
% 10/5/2015

% I have adhered to all the tenets of the 
% Duke Coddmmunity Standard in creating this code.
% Signed: th174
%%
clear;
format long e;
price = @(q) 6./1.1-(q>0).*q./(1.1e6);
revenue = @(q) q .* (price(q)>0).*price(q);
expense = @(q) 2.045e6 +.62.*q+.24.*q+.16.*q;
q = linspace(0,6*10^6,1000);
xq = [5.15662366e5,4.3662338534e6];
hold on
grid on
title 'Costs and Revenues as a function of Production (th174)'
xlabel 'Quantity Sold'
ylabel 'Dollar value'
plot(q,revenue(q),'k-',q,expense(q),'k--');
plot(xq,revenue(xq),'ks','MarkerSize',20);
legend('Revenue','Expenses','Breakeven Points');
print -deps BreakEvenPoint
profit = @(q) revenue(q)-expense(q);
Q = 0:6e6;
profits=profit(Q);
[max, maxi] = max(profits);
[min, mini] = min(profits);
maxi = maxi-1
max
mini = mini-1
min