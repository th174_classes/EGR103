% CustomerMap.m
% Tianyue Huang
% 10/5/2015

% I have adhered to all the tenets of the 
% Duke Coddmmunity Standard in creating this code.
% Signed: th174
%%
clear;
format short e
data = load('DataTable.dat');
customers = data(:,1);
xPos = data(:,2);
yPos = data(:,3);
volumes = data(:,4);
figure(1); clf
text(xPos,yPos,num2str(customers));
axis([-30 30 -30 30]);
grid on
xlabel 'x (miles)'
ylabel 'y (miles)'
title 'Customer Locations (th174)'
print -depsc CustomerMap1
%%
figure(2); clf
[x,y] = meshgrid(-30:2:30,-30:2:30);
totalcosts = zeros(31);
for i=customers'
distances = hypot(xPos(i)-x,yPos(i)-y);
costs = 0.5.*distances.*volumes(i);
totalcosts = totalcosts + costs;
end
meshc(x,y,totalcosts);
colormap copper
xlabel 'x (miles)'
ylabel 'y (miles)'
zlabel 'Total Delivery Cost'
title 'Total Costs to Deliver from each Location (th174)'
print -depsc CustomerMap2
%%
figure(3); clf
surf(x,y,totalcosts);
shading interp
colormap copper
colorbar;
axis equal
xlabel 'x (miles)'
ylabel 'y (miles)'
zlabel 'Total Delivery Cost'
title 'Total Costs to Deliver from each Location (th174)'
view(2);
print -depsc CustomerMap3
%%
figure(4); clf
contour(x,y,totalcosts,10);
axis equal
colormap copper
xlabel 'x (miles)'
ylabel 'y (miles)'
zlabel 'Total Delivery Cost'
title 'Total Costs to Deliver from each Location (th174)'
print -depsc CustomerMap4