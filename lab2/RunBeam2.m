 % RunBeam2.m
 % Tianyue Huang
 % 9/11/2015
 % Based on: RunCan.m
 % Written by: Michael R. Gustafson II (mrg@duke.edu)
 clear
format short e
load Beam2.dat
Mass = Beam2(:,1);
Displacement = Beam2(:,2);
Force = Mass*9.81;
Displacement = (Displacement*2.54)/100;
P = polyfit(Force, Displacement, 1)
ForceModel = linspace(min(Force),max(Force),100);
DispModel = polyval(P, ForceModel);
figure(1)
clf
plot(Force, Displacement, 'ko')
hold on
plot (ForceModel, DispModel, 'k-')
hold on
grid on
xlabel('Force (Newtons)')
ylabel('Displacement (meters)')
title('Displacement vs. Force for Beam2.dat (th174)')
    print -deps RunCanPlot
    % I have adhered to all the tenets of the 
 % Duke Community Standard in creating this code.
 % Signed: th174