% RunWeather.m
% Tianyue Huang
% 09/18/2015

% I have adhered to all the tenets of the
% Duke Community Standard in creating this code.
% Signed: th174

%% Initialize workspace
clear;
format short e;
%% Load data
MyData = load('NRELECSU2013.dat');

%% Split data into vectors whose names make sense
Day  = MyData(:,1);
Hour = MyData(:,2);
Irr  = MyData(:,3);
Temp = MyData(:,4);
Wind = MyData(:,5);

%% Analyze data
%  Find which element has the max irradiance, where it is in
%  the vector, and use that to print information about its value,
%  and the day and hour it happened
%  Add code here to figure out what the max value is - call
%  that Val - and in which element - call that Loc - then use that
%  in the fprintf below
[Val, Loc] = max(Irr);
fprintf('Max irradiance of %+0.2e W/m^2 on day %3.0f and hour %2.0f\n',...
    Val, Day(Loc), Hour(Loc))

%  Find the value and element location of the max avg. hourly temp,
%  then print out that information
[Val, Loc] = max(Temp);
fprintf('Max avg. temp. of %+0.2e deg C on day %3.0f and hour %2.0f\n',...
    Val, Day(Loc), Hour(Loc))

%  Find the value and element location of the min avg. hourly temp,
%  then print out that information
[Val, Loc] = min(Temp);
fprintf('Max avg. temp. of %+0.2e deg C on day %3.0f and hour %2.0f\n',...
    Val, Day(Loc), Hour(Loc))
%  Find the value and element location of the max avg. hourly wind speed,
%  then print out that information
[Val, Loc] = max(Wind);
fprintf('Max wind speed of %+0.2e m/sec on day %3.0f and hour %2.0f\n',...
    Val, Day(Loc), Hour(Loc))
%% Determine how many hours had a wind speed of zero and report that
NoWind = 0;
for i=1:length(Wind)
    NoWind = NoWind + (Wind(i) == 0);
end
fprintf('Average wind speed was 0 m/sec for %4.0f hours\n', NoWind);
%% Determine how many hours the temp was below 0C and report that
timeFrozen = 0;
for i=1:length(Temp)
    timeFrozen = timeFrozen + (Temp(i) < 0);
end
fprintf('Avg temperature was below zero for %4.0f hours\n', timeFrozen);
%% Plot data
%  Post-Noon-Hour Irradiance for 2013
figure(1), clf
plot(Day(13:24:end),Irr(13:24:end),'k-');
xlabel 'Day of Year'
ylabel 'Irradiance, W/m^2'
title 'Post-Noon-Hour Irradiance for 2013 (th174)'
print -deps WeatherPlot1

%  Hourly Average Temperatures for January 2013
figure(2), clf
plot((1:744),Temp(1:744),'k-');
xlabel 'Time in January (Hours)'
ylabel 'Temperature, degrees C'
title 'Hourly Average Temperatures for Janurary 2013 (th174)'
print -deps WeatherPlot2

%  25 Fastest Average Hourly Wind Speeds
figure(3), clf
sortedWind = sort(Wind,'descend');
bar((1:25), sortedWind(1:25),'k');
xlabel 'Rank' ;
ylabel 'Wind Speed, m/sec'
title '25 Fastest Average Hourly Wind Speeds'
print -deps WeatherPlot3