%% IMPORTANT: Once working, run latex 3 times to get listoffigures to work

%% Be sure to check spelling!

%% Put **your** name and the proper due date in place (but without the ****)

%% Copy the lstlisting and figure code as many times as you need
%% Be sure to put in your own file names if appropriate

%% Note that the \epsfig commands are currently commented out - until the
%%%% files exist, processing this code without them will result in an error
%%%% so leave the comments until you have created the graphics files!

\documentclass{article}
\usepackage{array}      % allows m{width} in tabulars for vertical centering
\usepackage{amsmath}    % load AMS-Math package
\usepackage{epsfig}     % allows PostScript files
\usepackage{listings}   % allows lstlisting environment
\usepackage{moreverb}   % allows listinginput environment
\usepackage{vmargin}    % allows better margins
\setpapersize{USletter} % sets the paper size
\setmarginsrb{1in}{0.5in}{1in}{0.2in}{12pt}{11mm}{0pt}{11mm} %sets margins 
\begin{document}
\begin{center}
\rule{6.5in}{0.5mm}\\~\\
{\bf \large EGR 103L - Fall 2015}\\~\\
{\huge \bf Laboratory 3 - Program Control and Functions}\\~\\
Tianyue Huang (th174)\\
Lab Section EGR103L9-04, Wednesday, 8:30 - 11:20 AM\\
Friday 9/4 - 8PM, 2015\\~\\ 
{\small I understand and have adhered to all the tenets of the Duke
  Community Standard in completing every part of this assignment.  I
  understand that a violation of any part of the Standard on any part
  of this assignment can result in failure of this assignment, failure
  of this course, and/or suspension from Duke University.} 

~\\Tianyue Huang~\\~\\
\rule{6.5in}{0.5mm}\\
\end{center}
\tableofcontents
\listoffigures
\pagebreak

\section{Based on Chapra Problem 2.11}
The equation very closely models the data set. There are three points just above the model, and three points just below. The equation is very accurate in representing the concentration of bromine over time.

\section{Based on Chapra Problem 2.22}
The equations for the butterfly curve\cite[p.~47]{Chapra} are:
\begin{align*}
x = \sin(t)\Bigg(e^{\cos(t)} - 2 \cos 4t - \sin^5 \frac{t}{12}\Bigg)\\
y = \cos(t)\Bigg(e^{\cos(t)} - 2 \cos 4t - \sin^5 \frac{t}{12}\Bigg)
\end{align*}
The butterfly plot is so named because its graph so closely resembles the image of a butterfly. Multiplying $(x,y)$ by $(\sin(t),\cos(t))$ respectively converts a polar graph into a parametric graph with $t = \theta$.
The curve $r = \big(e^{\cos(t)} - 2 \cos 4t - \sin^5 \frac{t}{12}\big)$ is responsible for the petals on the butterfly plot that give it its distinct shape.
The exponential term is reponsible for the relative sizes of each of the petals that make up the butterfly wing, with two of them dominating the graph. The second term is reponsible for the petals themselves, which are modified rose curves.
The last term in $\big(e^{\cos(t)} - 2 \cos 4t - \sin^5 \frac{t}{12}\big)$ is responsible for the many layers of the curve that form the butterfly plot, by slightly modifying the size of the curve each revolution.

\section{Chapra Problem 3.10}
The minimum displacement on the beam should be -1.0531e+02, which occurs at approximately $x = 2.32 feet$.
The maximum displacement on the beam should be 4.3170e+01, which occurs at approximately $x = 5.96 feet$.
The final plot is in the Appendix. 
The full text of the function and script are in the Appendix

\section{Palm Problem 4.44 - Pressures}
\renewcommand{\arraystretch}{1.3}
The $a$ and $b$ values for six gases are, from
references \cite{Palm} and \cite{Other} and alphabetically by gas name:
\begin{center}
\begin{tabular}{ccc}\hline
\textbf{Gas} & \textbf{a (L$^2$-atm/mol)} & \textbf{b (L/mol)}\\
Hydrogen, H & 0.244&0.0266\\
Helium, He & 0.0341&0.0237\\
Water vapour, H$_2$O&5.536&0.03049 \\
Carbon dioxide, CO$_2$&3.59&0.0427\\
Oxygen, O$_2$&1.36&0.0318\\
Chlorine, Cl$_2$&6.49&0.0562\\
\end{tabular}
\end{center}
A graph of pressures for the above gases at $T=$300K and specific
volumes $\hat{V}$ between 1 and 2 L/mol is in Figure
\ref{PressureGraph} on page \pageref{PressureGraph}.  The pressure for
each gas comes from the following function given in Palm
\cite[p.~215]{Palm}:  
\begin{align*}
P = \frac{RT}{\hat{V}-b}-\frac{a}{\hat{V} ^2}
\end{align*}
\pagebreak
\section{Weather Data Analysis}
Irradiance in the hour after noon generally followed the cycle of the seasons, with the most intense irradiance around the summer solstice, the least intense irradiance around the winter solstice, and a fairly gradual change in between. There are a number of 
Table \ref{WeatherTest}
% and
page \pageref{WeatherTest}

\pagebreak
\appendix
\section{Codes}
% Put the name of your file in the subsection name 
% and the listinginput input
% Be sure to include the community standard in codes!

% Add or remove \pagebreaks if they make sense

% Put the files in the same order as the problems; generally, 
% scripts will come first followed by any functions called
% by those scripts.


\subsection{RunConc.m}
\listinginput[1]{1}{RunConc.m}
\pagebreak

\subsection{RunButterfly.m}
\listinginput[1]{1}{RunButterfly.m}
\pagebreak

\subsection{Butterfly.m}
\listinginput[1]{1}{Butterfly.m}
\pagebreak

\subsection{TestSingularity.m}
\listinginput[1]{1}{TestSingularity.m}
\pagebreak

\subsection{BeamDisplacement.m}
\listinginput[1]{1}{BeamDisplacement.m}
\pagebreak

\subsection{GraphPressures.m}
\listinginput[1]{1}{GraphPressures.m}
\pagebreak

\subsection{VanDerWaals.m}
\listinginput[1]{1}{VanDerWaals.m}
\pagebreak

\subsection{RunWeather.m}
\listinginput[1]{1}{RunWeather.m}


\clearpage % start Diary on new page

\section{Diary}
\begin{table}[htb!]
\listinginput[1]{1}{WeatherDiary.txt}
\begin{center}
\caption{Output from TestWeather.m\label{WeatherTest}}
\end{center}
\end{table}

\clearpage % start Figures on new page

\section{Figures}

\begin{figure}[ht!]
\begin{center}
\epsfig{file=ConcPlot.eps, width=4.5in}
\caption{Plot for Chapra 2.11.\label{ConcGraph}}
\end{center}
\end{figure}

\begin{figure}[ht!]
\begin{center}
\epsfig{file=ButterflyPlot.eps, width=4.5in}
\caption{Butterfly Curve.\label{ButterflyGraph}}
\end{center}
\end{figure}

\begin{figure}[ht!]
\begin{center}
\epsfig{file=PressuresPlot.eps, width=4.5in}
\caption{VanDerWaals Calculations Plots.\label{PressureGraph}}
\end{center}
\end{figure}

\begin{figure}[ht!]
\begin{center}
\epsfig{file=SingPlots.eps, width=4.5in}
\caption{Test of Singularity function.}
\end{center}
\end{figure}

\begin{figure}[ht!]
\begin{center}
\epsfig{file=DisplacementPlot.eps, width=4.5in}
\caption{Displacement plot for a beam.}
\end{center}
\end{figure}

\begin{figure}[ht!]
\begin{center}
\epsfig{file=WeatherPlot1.eps, width=4.5in}
\caption{Midday Irradiance for 2013.\label{Weather1Graph}}
\end{center}
\end{figure}

\begin{figure}[ht!]
\begin{center}
\epsfig{file=WeatherPlot2.eps, width=4.5in}
\caption{Hourly Temperatures for January 2013.\label{Weather2Graph}}
\end{center}
\end{figure}

\begin{figure}[ht!]
\begin{center}
\epsfig{file=WeatherPlot3.eps, width=4.5in}
\caption{Top Average Windspeeds for 2013.\label{Weather3Graph}}
\end{center}
\end{figure}
\clearpage

\begin{thebibliography}{9}
\bibitem{Chapra}
  Chapra, Steven C.,
  {\it Applied Numerical Methods with MATLAB for Engineering and Scientists}.
  McGraw-Hill, New York,
  3rd Edition,
  2012.
\bibitem{Palm}
  Palm, William J.,
  {\it Introduction to MATLAB for Engineers}.
  McGraw-Hill, New York,
  3rd Edition,
  2011.
\bibitem{Other}
  Weast. R. C. (Ed.), \textit{Handbook of Chemistry and Physics}. Cleveland:Chemical Rubber Co., Ohio, 53rd Edition, 1972.
\end{thebibliography}

\end{document}


% LocalWords:  Chapra 
